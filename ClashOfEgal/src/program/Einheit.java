package program;

public class Einheit {

	private String name;
	private int kosten;
	private String bildpfad;

	public Einheit(String name, int kosten, String bildpfad) {
		this.name = name;
		this.kosten = kosten;
		this.bildpfad = bildpfad;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKosten() {
		return kosten;
	}

	public void setKosten(int kosten) {
		this.kosten = kosten;
	}

	public String getBild() {
		return bildpfad;
	}

	public void setBild(String bildpfad) {
		this.bildpfad = bildpfad;
	}	
	
	@Override
	public String toString() {
		return "Einheit [name=" + name + ", kosten=" + kosten + ", bild=" + bildpfad + "]";
	}
}



