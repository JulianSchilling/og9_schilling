package test;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.DropMode;

public class Frame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame frame = new Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("Kaufen");
		btnNewButton.setBounds(10, 375, 110, 75);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JButton button =(JButton)e.getSource();

				JOptionPane.showMessageDialog(button,
				    "Du hast "+" gekauft");
			}
		});
		contentPane.setLayout(null);
		contentPane.add(btnNewButton);
		
		JButton button = new JButton("->");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			//	JButton button =(JButton)arg0.getSource();
			
			//	JOptionPane.
			}
		});
		button.setBounds(130, 11, 44, 439);
		contentPane.add(button);
		
		JLabel label = new JLabel("...");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(33, 218, 46, 24);
		contentPane.add(label);
		
		JLabel lblIcon = new JLabel("");
		lblIcon.setForeground(Color.WHITE);
		lblIcon.setBackground(Color.GRAY);
		lblIcon.setBounds(23, 103, 63, 59);
		contentPane.add(lblIcon);
		
		JLabel lblGold = new JLabel("");
		lblGold.setBackground(Color.GRAY);
		lblGold.setBounds(23, 259, 63, 59);
		contentPane.add(lblGold);
	}
}
