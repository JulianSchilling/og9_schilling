
import java.util.Scanner;
 
public class BMIrechner {
 
    public static void main(String[] args) {
 
        Scanner myScanner = new Scanner(System.in);
         
        System.out.println("Willkommen beim BMI Rechner");
        System.out.println("Bitte gib dein Gewicht in KG ein:");						// Eingabe der Gr��e und des Gewichtes
        double gewicht = myScanner.nextDouble();
        System.out.println("Bitte gib deine Gr��e in Meter an (Bsp 1,80):");
        double groesse = myScanner.nextDouble();
         
        double bmi = bmiRechner(groesse, gewicht);				//BMI wird mithilfe der Methode "unten" ausgerechnet
        ausgabe(bmi);
    }
 
    private static void ausgabe(double bmi) {       
        System.out.print("Dein BMI betr�gt: " + bmi + " ");
        if(bmi <16) {
            System.out.println("Starkes Untergewicht!");
        }else if(bmi>=16 && bmi<17) {
            System.out.println("M��iges Untergewicht.");
        }else if(bmi>=17 && bmi<18.5) {
            System.out.println("Leichtes Untergewicht.");						//Ausgabe je nach BMI
        }else if(bmi>=18.5 && bmi<25) {
            System.out.println("Normalgewicht!");
        }else if(bmi>=25 && bmi<30) {
            System.out.println("Pr�adipositas.");
        }else if(bmi>=30 && bmi<35) {
            System.out.println("Adipositas Grad I.");
        }else if(bmi>=35 && bmi<40) {
            System.out.println("Adipositas Grad II");
        }else if(bmi>= 40) {
            System.out.println("Adipositas Grad III");
        }else {
            System.out.println("Bitte �berpr�fe deine Eingabe!");
        }
         
    }
 
    private static double bmiRechner(double groesse, double gewicht) {
        double bmi = gewicht / (groesse*groesse);									//BMI Rechner
        double d = Math.pow(10, 2);
        return Math.round(bmi * d) / d;
    }
}