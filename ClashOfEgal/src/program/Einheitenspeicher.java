package program;

public class Einheitenspeicher {

	private static Einheit[] einheiten;
	private static int counter = -1;
	
	/**
	 * Fill the list
	 */
	private static void initList() {
		einheiten = new Einheit[]{
				new Einheit("Barbar",5,"./src/bilder/Barbar.png"),
				new Einheit("Drachenbaby",3,"./src/bilder/Drachenbaby.png"),
				new Einheit("P.E.K.K.A",50,"./src/bilder/P.E.K.K.A.png")
		};
	}
	
	/**
	 * Gibt die n�chste Einheit zur�ck
	 * @return
	 */
	public static Einheit next() {
		if (einheiten == null) initList(); //liste f�llen?
		
		counter = (++counter)%einheiten.length;
		return einheiten[counter];
	}

}