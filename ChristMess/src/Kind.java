import java.util.*;

public class Kind{
	private String Bravheit;
	  private String name;
	  private String gebtag;
	  private String wohnort;
	  
	  public Kind(String Bravheit, String name) {
	    this.Bravheit =Bravheit;
	    this.name = name;
	  }

	  
	  public String getBravheit() {
	    return Bravheit;
	  }

	  public void setBravheit(String Bravheit) {
	    this.Bravheit = Bravheit;
	  }

	  public String getName() {
	    return name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }
	  

	  public String getGebtag() {
		return gebtag;
	}


	public void setGebtag(String gebtag) {
		this.gebtag = gebtag;
	}


	public String getWohnort() {
		return wohnort;
	}


	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}


	public boolean equals(Kind k){
	    return k.getBravheit().equals(this.Bravheit);
	  }
	  
	  public String toString(){
	    return "[ Bravheit: "+this.Bravheit + ", "+ "Name: "+this.name + " ]";
	  }
	  
	  public int compareTo(Kind k){
	    return this.Bravheit.compareTo(k.getBravheit());
	    
	  }
}