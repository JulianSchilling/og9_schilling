package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	double posX = (double)(Math.random() * 160);
	double posY = (double)(Math.random() * 50);
	int maxKapazitaet = 122;
	String typ = "Testflieger T2016";
	String antrieb = "Superantrieb";
	int winkel = 123;
	// Methoden

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMaxKapazitaet() {
		return maxKapazitaet;
	}

	public void setMaxKapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','^', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '[', 'X', ']', '\0'},
				{'\0', '[', 'X', ']', '\0'},
				{'/', '_', '^','_', '\\'},				
		};
		return raumschiffShape;
	}

}
