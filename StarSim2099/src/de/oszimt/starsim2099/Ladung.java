package de.oszimt.starsim2099;

/**
 * 
 * 
 * @author Julian
 * @version V1
 */
public class Ladung {

	// Attribute
	double posX = (double)(Math.random() * 160);
	double posY = (double)(Math.random() * 50);
	int masse = 122;
	String typ = "Test-Ladung (gepunktet)";
	// Methoden

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}