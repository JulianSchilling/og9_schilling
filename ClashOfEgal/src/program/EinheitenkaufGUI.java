package program;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class EinheitenkaufGUI extends JFrame {

	private static final long serialVersionUID = 1L; //nur damit eclipse nicht meckert
	private JPanel contentPane;
	private JLabel lblBild;
	private JLabel lblName;
	private JLabel lblKosten;
	private JButton btnKaufen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EinheitenkaufGUI frame = new EinheitenkaufGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EinheitenkaufGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 200, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblClashOfEgal = new JLabel("Clash of Egal");
		lblClashOfEgal.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblClashOfEgal.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblClashOfEgal, BorderLayout.NORTH);
		
		JPanel pnlEinheitenuebersicht = new JPanel();
		contentPane.add(pnlEinheitenuebersicht, BorderLayout.CENTER);
		pnlEinheitenuebersicht.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblBild = new JLabel("");
		lblBild.setHorizontalAlignment(SwingConstants.CENTER);
		pnlEinheitenuebersicht.add(lblBild);
		
		lblName = new JLabel("");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		pnlEinheitenuebersicht.add(lblName);
		
		lblKosten = new JLabel("");
		lblKosten.setHorizontalAlignment(SwingConstants.CENTER);
		lblKosten.setIcon(new ImageIcon("./src/bilder/Gold.png"));
		pnlEinheitenuebersicht.add(lblKosten);
		
		//erste Einheit abrufen und GUI f�llen
		naechsteEinheit();
		
		btnKaufen = new JButton("Kaufen");
		pnlEinheitenuebersicht.add(btnKaufen);
		
		//Weiter Knopf
		JButton btnSchliessen = new JButton("->");
		btnSchliessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				naechsteEinheit();
			}
		});
		contentPane.add(btnSchliessen, BorderLayout.EAST);
	}

	/**
	 * l�dt eine neue Einheit in die GUI
	 */
	public void naechsteEinheit(){
		Einheit e = Einheitenspeicher.next(); //n�chste Einheit abrufen
		lblBild.setIcon(new ImageIcon(e.getBild()));//Bild setzen
		lblName.setText(e.getName());//Name setzen
		lblKosten.setText(e.getKosten()+"");//Kosten setzen
	}
}
