import java.io.FileNotFoundException;
import java.net.URISyntaxException;
/* 
Ausnahmen. Einfach Ausnahmen.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //schreib hier deinen Code

        methode1();

        //schreib hier deinen Code
    }

    public static void methode1() throws NullPointerException, ArithmeticException, FileNotFoundException, URISyntaxException {
        int i = (int) (Math.random() * 4);
        if (i == 0)
            throw new NullPointerException();
        if (i == 1)
            throw new ArithmeticException();
        if (i == 2)
            throw new FileNotFoundException();
        if (i == 3)
            throw new URISyntaxException("", "");
    }
}