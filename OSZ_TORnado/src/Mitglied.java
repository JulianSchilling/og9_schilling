
public class Mitglied {
	String name ="";
	String telefonNr ="";
	boolean jahresbetrag = true;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonNr() {
		return telefonNr;
	}
	public void setTelefonNr(String telefonNr) {
		this.telefonNr = telefonNr;
	}
	public boolean isJahresbetrag() {
		return jahresbetrag;
	}
	public void setJahresbetrag(boolean jahresbetrag) {
		this.jahresbetrag = jahresbetrag;
	}
	
}
